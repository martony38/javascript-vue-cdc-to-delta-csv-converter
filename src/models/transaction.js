export class Transaction {
    static fiats = [
        "USD", "USDM", "EUR", "AUD", "BGN",
        "CAD", "HRK", "CZK", "DKK", "HKD",
        "HUF", "ILS", "JPY", "NZD", "NOK",
        "PLN", "RON", "SGD", "SAR", "SEK",
        "CHF", "GBP", "XSGD"
    ];

    // some of the cryptocurrency require a name change to map correctly, I may not know all and do raise an issue if you find one
    static convert = {
        USDM: "USD",
        XSGD: "SGD",
        FET: "FET*",
        COS: "COS*",
        TCAD: "CAD"
    };

    date = "";
    type = "";
    exchange = "";
    baseAmount = "";
    baseCurrency = "";
    quoteAmount = "";
    quoteCurrency = "";
    sync = "1";
    from = "";
    to = "";
    notes = "";

    trade(date, baseAmount, baseCurrency, quoteAmount, quoteCurrency) {
        this.date = date;

        let isBaseFiat = Transaction.fiats.includes(baseCurrency);
        let isQuoteFiat = Transaction.fiats.includes(quoteCurrency);

        // ignore fiat to fiat
        if (isBaseFiat && isQuoteFiat)
            return;

        baseAmount = Transaction.nonZero(Math.abs(baseAmount))
        quoteAmount = Transaction.nonZero(Math.abs(quoteAmount))

        if (isBaseFiat) {
            this.type = "SELL";
            this.baseAmount = quoteAmount;
            this.baseCurrency = quoteCurrency;
            this.quoteAmount = baseAmount;
            this.quoteCurrency = baseCurrency;
            this.sync = "0";
        } else {
            this.type = "BUY";
            this.baseAmount = baseAmount;
            this.baseCurrency = baseCurrency;
            this.quoteAmount = quoteAmount;
            this.quoteCurrency = quoteCurrency;
            if (isQuoteFiat)
                this.sync = "0";
        }
    }

    deposit(date, baseAmount, baseCurrency) {
        this.type = "DEPOSIT";
        this.date = date;
        this.baseAmount = Transaction.nonZero(baseAmount);
        this.baseCurrency = baseCurrency;
        this.from = "OTHER";
        this.to = "MY_WALLET";
    }

    withdraw(date, baseAmount, baseCurrency) {
        this.type = "WITHDRAW";
        this.date = date;
        this.baseAmount = Transaction.nonZero(Math.abs(baseAmount));
        this.baseCurrency = baseCurrency;
        this.from = "MY_WALLET";
        this.to = "OTHER";
    }

    transfer(date, from, to, baseAmount, baseCurrency) {
        this.type = "TRANSFER";
        this.date = date;
        this.baseAmount = Transaction.nonZero(Math.abs(baseAmount));
        this.baseCurrency = baseCurrency;
        this.from = from;
        this.to = to;
    }

    translate() {
        if (this.baseCurrency in Transaction.convert)
            this.baseCurrency = Transaction.convert[this.baseCurrency];
        if (this.quoteCurrency in Transaction.convert)
            this.quoteCurrency = Transaction.convert[this.quoteCurrency];
    }

    toString() {
        return this.date + ','
            + this.type + ','
            + this.exchange + ','
            + this.baseAmount + ','
            + this.baseCurrency + ','
            + this.quoteAmount + ','
            + this.quoteCurrency + ','
            + ",,,,"
            + this.sync + ','
            + this.from + ','
            + this.to + ','
            + this.notes + '\n';
    }

    static nonZero(amount) {
        return amount === 0 ? "0.00000009" : amount
    }
}
